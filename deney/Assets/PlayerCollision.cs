﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using EZCameraShake;

public class PlayerCollision : MonoBehaviour
{
    private GameManager gm;
    public GameObject ReplayButton;
    private GameObject Player;
    public GameObject effect;
    private Slider slider;
    private GameObject Grounds;
    void Start()
    {
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Grounds = GameObject.FindGameObjectWithTag("GroundManager");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Instantiate(effect, Player.transform.position+new Vector3(0,2f,-2f), Quaternion.identity);
            gameObject.GetComponent<Renderer>().enabled= false;
            CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
            gm.speedInc = 0f;
            gm.countInc = 0f;
            gm.sliderCount = 0f;
            StartCoroutine(Wait());
        }

    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gm.count = 0f;
        slider.value = 0f;
        Grounds.transform.position = Grounds.GetComponent<move>().playerPos;
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject ContinueButton = Instantiate(ReplayButton, new Vector3(0, 130, 1), Quaternion.identity) as GameObject;
        ContinueButton.transform.SetParent(Canvas.transform, false);
    }
}
