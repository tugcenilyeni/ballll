﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject player;
    public Vector3 camPos;
    private float distance;

    
    void Start()
    {
        distance = 25f;
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void FixedUpdate()
    {
        camPos = new Vector3(0, 12f, player.transform.position.z - distance);
        gameObject.transform.position = camPos;
    }
}
