﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    private GameManager gm;
    public GameObject contButton;
    private GameObject Grounds;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        Grounds = GameObject.FindGameObjectWithTag("GroundManager");
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            gm.speedInc = 0f;
            gm.countInc = 0f;
            StartCoroutine(Wait());
        }
        
    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        if (gm.amnGroundOnScreen < 24)
        {
            gm.amnGroundOnScreen += 2;
        }
        if (gm.speed < 120f)
        {
            gm.speed += 10f;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Grounds.transform.position = Grounds.GetComponent<move>().playerPos;
        if(gm.level < 250)
        {
            gm.level += 1;
        }
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject ContinueButton = Instantiate(contButton, new Vector3(0, 130, 1), Quaternion.identity) as GameObject;
        ContinueButton.transform.SetParent(Canvas.transform, false);
    }
}
